class Note {
  int? id;
  String title;
  DateTime date;
  String description;

  Note({this.id, required this.title, required this.date, required this.description});
}
