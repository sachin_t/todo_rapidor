import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import '../model/notes_model.dart';

class DatabaseHelper {
  static Database? _database;

  Future<Database?> get database async {
    if (_database != null) {
      return _database;
    }
    _database = await _initDatabase();
    return _database;
  }

  Future<Database> _initDatabase() async {
    final String databasesPath = await getDatabasesPath();
    final String path = join(databasesPath, 'notes.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: (Database db, int version) async {
        await db.execute('''
          CREATE TABLE notes (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            title TEXT,
            date TEXT,
            description TEXT
          )
        ''');
      },
    );
  }

  Future<List<Note>> getNotes({required int limit, required int offset}) async {
    final Database? db = await database;
    final List<Map<String, dynamic>> maps = await db!.query(
      'notes',
      limit: limit,
      offset: offset,
    );
    return List.generate(maps.length, (index) {
      return Note(
        id: maps[index]['id'],
        title: maps[index]['title'],
        date: DateTime.parse(maps[index]['date']),
        description: maps[index]['description'],
      );
    });
  }

  Future<void> insert(Note note) async {
    final Database? db = await database;
    await db!.insert(
      'notes',
      {
        'title': note.title,
        'date': note.date.toIso8601String(),
        'description': note.description,
      },
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> delete(int id) async {
    final Database? db = await database;
    await db!.delete(
      'notes',
      where: 'id = ?',
      whereArgs: [id],
    );
  }
}
