import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../controller/notes_controller.dart';
import '../model/notes_model.dart';

class NoteView extends StatelessWidget {
  final _noteController = Get.put(NoteController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.deepPurple,
          title: const Text('To Do List',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                  fontSize: 22)),
        ),
        body: _toDoList(),
        floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.add),
          onPressed: () => _showAddNoteDialog(context),
        ),
        bottomNavigationBar: Obx(
          () => Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              IconButton(
                icon: const Icon(Icons.chevron_left),
                onPressed: _noteController.previousPage,
              ),
              Text('Page ${_noteController.currentPage}'),
              IconButton(
                icon: const Icon(Icons.chevron_right),
                onPressed: _noteController.nextPage,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget? _toDoList() {
    return Obx(
      () => ListView.builder(
        itemCount: _noteController.notes.length,
        itemBuilder: (context, index) {
          final Note note = _noteController.notes[index];
          return Card(
            margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            elevation: 8,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 8.0, vertical: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0, top: 8),
                        child: Text(note.title,
                            style: const TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w500)),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0, bottom: 5),
                        child: Text(note.description,
                            style: const TextStyle(
                                fontSize: 16,
                                color: Colors.grey,
                                fontWeight: FontWeight.w500)),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 10.0,
                        ),
                        child: Text(DateFormat('MM-dd-yyyy')
                            .format(note.date)
                            .toString()
                            .replaceAll('-', '/')),
                      ),
                    ],
                  ),
                ),
                const Spacer(),
                IconButton(
                  icon: const Icon(Icons.delete, color: Colors.redAccent),
                  onPressed: () => _noteController.deleteNote(note.id!),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  void _showAddNoteDialog(BuildContext context) {
    final TextEditingController titleController = TextEditingController();
    final TextEditingController descriptionController = TextEditingController();
    Get.dialog(
      AlertDialog(
        title: const Text('Add Note'),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextField(
              controller: titleController,
              decoration: const InputDecoration(labelText: 'Title'),
            ),
            TextField(
              controller: descriptionController,
              decoration: const InputDecoration(labelText: 'Description'),
              maxLength: 300,
            ),
          ],
        ),
        actions: [
          TextButton(
            child: const Text('Cancel'),
            onPressed: () => Get.back(),
          ),
          TextButton(
            child: const Text('Add'),
            onPressed: () {
              final String title = titleController.text;
              final String description = descriptionController.text;
              final DateTime date = DateTime.now();
              _noteController.addNote(
                title: title,
                date: date,
                description: description,
              );
              Get.back();
            },
          ),
        ],
      ),
    );
  }
}
