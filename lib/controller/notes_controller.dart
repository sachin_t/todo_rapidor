import 'package:get/get.dart';
import '../model/notes_model.dart';
import '../database_service/database_helper.dart';

class NoteController extends GetxController {
  final _databaseHelper = DatabaseHelper().obs;
  final _notes = <Note>[].obs;
  final currentPage = 1.obs;
  final _perPage = 10;

  List<Note> get notes => _notes.toList();

  @override
  void onInit() {
    super.onInit();
    loadNotes();
  }

  void loadNotes() async {
    final List<Note> notes = await _databaseHelper.value.getNotes(
      limit: _perPage,
      offset: (currentPage.value - 1) * _perPage,
    );
    _notes.value = notes;
  }

  void addNote({
    required String title,
    required DateTime date,
    required String description,
  }) async {
    final note = Note(
      title: title,
      date: date,
      description: description,
    );
    await _databaseHelper.value.insert(note);
    loadNotes();
  }

  void deleteNote(int id) async {
    await _databaseHelper.value.delete(id);
    loadNotes();
  }

  void nextPage() {
    currentPage.value++;
    loadNotes();
  }

  void previousPage() {
    if (currentPage.value > 1) {
      currentPage.value--;
      loadNotes();
    }
  }
}
